export const hrefHome: string = `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/`;
export const hrefDerouleFormulaire: string = `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/deroule/formulaire`;
export const hrefDerouleType: string = `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/deroule/type`;
export const hrefBibliotheque: string = `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/bibliotheque`;
export const hrefFiche: string = `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/bibliotheque/fiche`;
export const hrefConnexion: string = `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/connexion`;
export const hrefDocumentation: string = `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/documentation`;
export const hrefFaq: string = `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/faq`;
