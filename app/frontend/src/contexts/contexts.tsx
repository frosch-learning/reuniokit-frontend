// Type
import { FormData } from "@/src/types/form";

// Components and hooks
import { createContext } from "react";

export const formDataContext = createContext<any>(null);
export const stepContext = createContext<any>(null);
