"use client";

// Types
import { FicheSynthese } from "@/src/types/fiche";
import { FilterGroup } from "@/src/types/filtre";

// Style
import styles from "./bibliotheque.module.scss";

// Components
import CardLibrary from "@/src/components/Cards/CardLibrary/CardLibrary";

// Lib
import React, { useState, useEffect } from "react";
import Filtre from "@/src/components/Filtre/Filtre";
import Breadcrumb from "@codegouvfr/react-dsfr/Breadcrumb";

const book_icon = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/icon_library.svg`;

export default function Bibliotheque() {
  const isAdmin = false;
  const [fiches, setFiches] = useState<FicheSynthese[]>([]);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [searchResults, setSearchResults] = useState<FicheSynthese[]>([]);
  const [filterGroups, setFilterGroups] = useState<FilterGroup[]>([]);
  const [selectedFilters, setSelectedFilters] = useState<{
    [key: string]: string;
  }>({});

  const fetchFiche = async () => {
    try {
      const responseFiches = await fetch(
        process.env.NEXT_PUBLIC_API_URL + "/api/v1/fiche/synthese",
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
        },
      );

      if (!responseFiches.ok) {
        throw new Error("Erreur lors de la récupération des fiches");
      }

      const fichesData = await responseFiches.json();

      setFiches(fichesData.results);
      setSearchResults(fichesData.results);

      console.log("Fiches chargées:", fichesData); // Juste après les avoir chargées
    } catch (error) {
      console.error(error);
    }
  };

  const fetchFiltres = async () => {
    try {
      const responseFilters = await fetch(
        process.env.NEXT_PUBLIC_API_URL + "/api/v1/fiche/filtre",
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
        },
      );
      if (!responseFilters.ok) {
        throw new Error("Erreur lors de la récupération des filtres");
      }

      const filtersData = await responseFilters.json();
      console.log("Filtres chargés:", filtersData); // Juste après avoir chargé les filtres

      const generatedFilterGroups: FilterGroup[] = [
        {
          label: "Objectifs",
          options: filtersData.objectifs.map((obj: string) => ({
            label: obj,
            value: obj,
          })),
        },
        {
          label: "Modalité",
          options: [
            { label: "Distanciel", value: true },
            { label: "Présentiel", value: false },
          ],
        },
        {
          label: "Difficulté",
          options: filtersData.difficulte.map((diff: string) => ({
            label: diff,
            value: diff,
          })),
        },
      ];

      setFilterGroups(generatedFilterGroups);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    if (isAdmin) {
      console.log("Is ADMIN!!");
    }
    fetchFiche();
    fetchFiltres();
  }, []);

  /* useEffect(() => {
    console.log("Fiches avant filtrage:", fiches);
    let filtered = fiches.filter(fiche => {
      const matchesSearchTerm = !searchTerm || fiche.nom.toLowerCase().includes(searchTerm.toLowerCase());
      console.log(`Fiche: ${fiche.nom}, Correspond au terme de recherche: ${matchesSearchTerm}`);

      const matchesObjectif = !selectedFilters['Objectifs'] || fiche.objectif.toLowerCase() === selectedFilters['Objectifs'].toLowerCase();
      console.log(`Fiche: ${fiche.nom}, Correspond à l'objectif: ${matchesObjectif}`);

      const matchesFaisableVisio = !selectedFilters['Faisable en visio'] || (fiche.modalite && fiche.modalite.trim().toLowerCase() === selectedFilters['Faisable en visio'].trim().toLowerCase());
      const matchesDifficulte = !selectedFilters['Difficulté'] || fiche.difficulte === selectedFilters['Difficulté'];

      return matchesSearchTerm && matchesObjectif && matchesFaisableVisio && matchesDifficulte;
    });
    console.log("Fiches après filtrage:", filtered);

    setSearchResults(filtered);
  }, [fiches, searchTerm, selectedFilters]); */

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const onFilterChange = (selectedValue: string, groupIndex: number) => {
    const groupLabel = filterGroups[groupIndex].label;
    console.log(
      "Valeur sélectionnée:",
      selectedValue,
      "pour le filtre:",
      groupLabel,
    );
    setSelectedFilters((prevFilters) => ({
      ...prevFilters,
      [groupLabel]: selectedValue,
    }));
  };

  if (!fiches) {
    return <div>Chargement des fiches</div>;
  }

  return (
    <>
      <div className={styles["breadcrumb-container"]}>
        <Breadcrumb
          currentPageLabel="Bibliothèque"
          homeLinkProps={{
            href: `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/`,
          }}
          segments={[]}
        />
      </div>
      <div className={styles["globalContainer"]}>
        <div className={styles["page-title"]}>
          <img src={book_icon} alt="" />
          <h1>Bibliothèque de séquences</h1>
        </div>
        <hr />
        <p className={styles["filter-title"]}>Filtres</p>
        <div className={styles["search-container"]}>
          <Filtre filterGroups={filterGroups} onFilterChange={onFilterChange} />

          <input
            type="text"
            placeholder="Rechercher..."
            value={searchTerm}
            onChange={handleSearchChange}
          />
        </div>

        <div className={styles["main-container-card"]}>
          {fiches.map((fiche, index) => (
            <CardLibrary key={index} detailSingleFiche={fiche} />
          ))}
        </div>
      </div>
    </>
  );
}
