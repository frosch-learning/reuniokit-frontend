"use client";

// Types
import { FicheComplement, FicheComplete } from "@/src/types/fiche";

// Style
import styles from "./fiche.module.scss"; // Assurez-vous d'utiliser le bon fichier SCSS

// Libs
import React, { useState, useEffect } from "react";
import { useSearchParams } from "next/navigation";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import Button from "@/src/components/Buttons/Button/Button";
import Badge from "@codegouvfr/react-dsfr/Badge";

// Assets
const separateur = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/separateur.svg`;
const map_pin = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/map_pin.svg`;
const timer = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/timer.svg`;
const team = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/team.svg`;
const flashlight = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/flashlight.svg`;
const star_full = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/star_full.svg`;
const star_empty = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/star_empty.svg`;

function listDictToList(
  keyToSwap: string,
  listDict: Array<FicheComplement>,
): Array<FicheComplement> {
  let results: FicheComplement[] = [];

  listDict.forEach((element) => {
    if (element.categorie === keyToSwap) {
      results.push(element);
    }
  });

  return results;
}

function renderPreparation(preparationInitiale: string) {
  if (!preparationInitiale) return null;

  const preparations = preparationInitiale.split(">").slice(1);
  if (preparations.length === 1) {
    return (
      <>
        <h2>Préparation</h2>
        <p>{preparations}</p>
      </>
    );
  }
  return (
    <>
      <h2>Préparation</h2>
      <ul>
        {preparations.map((prep, index) => (
          <li key={index}>{prep}</li>
        ))}
      </ul>
    </>
  );
}

function renderDeroulementPrecisions(precisions: string) {
  if (!precisions) return null;
  const precisionsList = precisions.replace(/[\[\]]/g, "").split(",");
  return (
    <>
      <ul>
        {precisionsList.map((precision, index) => (
          <li key={index}>{precision}</li>
        ))}
      </ul>
    </>
  );
}

function renderDeroulementExemples(exemples: string) {
  if (!exemples) return null;
  const exemplesList = exemples.replace(/[\[\]]/g, "").split(",");
  return (
    <>
      <p>Par exemples:</p>
      <ul>
        {exemplesList.map((exemple, index) => (
          <li key={index}>{exemple}</li>
        ))}
      </ul>
    </>
  );
}

export default function FichePage() {
  const searchParams = useSearchParams();
  const ficheId = searchParams.get("fiche");
  const [fiche, setFiche] = useState<FicheComplete>();

  const generatePdf = (nomFiche: string) => {
    const input = document.getElementById("content");
    const filename = nomFiche + ".pdf";

    if (input) {
      html2canvas(input, { scale: 2 }).then((canvas) => {
        const imgData = canvas.toDataURL("image/png");
        const pdf = new jsPDF("p", "mm", "a4");
        const margin = 2;

        const pdfWidth = pdf.internal.pageSize.getWidth();
        const pdfHeight = pdf.internal.pageSize.getHeight();

        const imgWidth = canvas.width * 0.264583;
        const imgHeight = canvas.height * 0.264583;

        const ratio = Math.min(
          (pdfWidth - 2 * margin) / imgWidth,
          (pdfHeight - 2 * margin) / imgHeight,
        );

        const canvasWidth = imgWidth * ratio;
        const canvasHeight = imgHeight * ratio;

        const x = margin;
        const y = margin;

        pdf.addImage(imgData, "PNG", x, y, canvasWidth, canvasHeight);
        pdf.save(filename);
      });
    }
  };

  const fetchFicheData = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/api/v1/fiche/${ficheId}`,
      );
      if (!response.ok) {
        throw new Error("Failed to fetch fiche data");
      }
      const data = await response.json();
      setFiche(data.result);
    } catch (error) {
      console.error("Error fetching fiche data:", error);
    }
  };

  function renderSection(title: string, content: string | string[]) {
    if (!content || content === "") return null;
    return (
      <>
        <h2>{title}</h2>
        <p>{content}</p>
      </>
    );
  }

  useEffect(() => {
    fetchFicheData();
  }, []);

  if (!fiche) {
    return <div>Chargement de la fiche...</div>;
  }

  return (
    <div id="content" className={styles["fiche"]}>
      <div className={styles["fiche-top"]}>
        <p>Séquence</p>
        <h2>{fiche.nom}</h2>
        <hr />
        <div className={styles["fiche-top__information"]}>
          <div className={styles["fiche-top__information-left"]}>
            <Badge className={styles["badge"]}>
              <img src={flashlight} alt="éclair" />
              {fiche.objectif.objectif}
            </Badge>
            <p className={styles["difficulte"]}>
              Difficulté
              <>
                {Array(Number(fiche.difficulte))
                  .fill(null)
                  .map((_, i) => (
                    <img key={i} src={star_full} alt="étoile pleine" />
                  ))}
                {Array(3 - Number(fiche.difficulte))
                  .fill(null)
                  .map((_, i) => (
                    <img key={i} src={star_empty} alt="étoile vide" />
                  ))}
              </>
            </p>
          </div>
          <img src={separateur} alt="" />
          <div className={styles["fiche-top__information-center"]}>
            <p>
              <img src={map_pin} alt="" />
              {fiche.compatible_visio ? (
                <>Présentiel / Distanciel</>
              ) : (
                <>Présentiel</>
              )}
            </p>
            <p>
              <img src={timer} alt="" />
              <span>Préparation :</span>
              {fiche.temps_preparation ? (
                fiche.temps_preparation
              ) : (
                <>non-renseignée</>
              )}
            </p>
            <p>
              <img src={timer} alt="" />
              <span>Animation :</span>
              {fiche.duree_max} min
            </p>
            <p>
              <img src={team} alt="" />
              {fiche.participant_max} participants
            </p>
          </div>
          <img src={separateur} alt="" />
          <div className={styles["fiche-top__information-right"]}>
            <p>Matériel</p>
            <ul>
              {listDictToList("materiel", fiche.fiche_complement).map(
                (item, index) => (
                  <li key={index}>{item.contenu}</li>
                ),
              )}
            </ul>
          </div>
        </div>
        <hr />
      </div>
      <div className={styles["fiche-middle"]}>
        {renderSection("Description", fiche.description_longue)}
        {renderPreparation(fiche.preparation)}
        <h2>Déroulement</h2>
        <ul>
          {listDictToList("deroulement", fiche.fiche_complement).map(
            (etape, index) => (
              <li key={index}>
                <p>{etape.contenu}</p>
                <p>{renderDeroulementPrecisions(etape.precisions)}</p>
                <p>{renderDeroulementExemples(etape.exemples)}</p>
              </li>
            ),
          )}
        </ul>
        {renderSection("Conseil", fiche.conseil)}
        {renderSection("Points de vigilance", fiche.points_de_vigilance)}
        {renderSection("A savoir", fiche.a_savoir)}
        <hr />
      </div>

      <div className={styles["fiche-footer"]}>
        <Button
          icon={process.env.NEXT_PUBLIC_ASSETS + "/details/file-download.svg"}
          className="label-btn"
          text={"Télécharger la fiche PDF"}
          onClick={() => generatePdf(fiche.nom)}
          buttonType={"primary"}
        />
      </div>
    </div>
  );
}
