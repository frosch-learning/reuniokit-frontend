// !!!! Mandatory !!!!
import { DsfrHead } from "@codegouvfr/react-dsfr/next-appdir/DsfrHead";
import { DsfrProvider } from "@codegouvfr/react-dsfr/next-appdir/DsfrProvider";
import { getHtmlAttributes } from "@codegouvfr/react-dsfr/next-appdir/getHtmlAttributes";
import { StartDsfr } from "./StartDsfr";
import { defaultColorScheme } from "./defaultColorScheme";
import Link from "next/link";

// Style
import "./globals.css";
import "@/src/styles/globals.scss";

// Components
import CustomHeader from "@/src/components/LayoutComponents/CustomHeader/CustomHeader";
import CustomFooter from "@/src/components/LayoutComponents/CustomFooter/CustomFooter";
import { Suspense } from "react";

export const metadata = {
  title: "Reuniokit",
  description: "Réalisé par DSCI",
};

export default function RootLayout({ children }: { children: JSX.Element }) {
  //NOTE: The lang parameter is optional and defaults to "fr"
  const lang = "fr";
  return (
    <html {...getHtmlAttributes({ defaultColorScheme, lang })}>
      <head>
        <StartDsfr />
        <DsfrHead Link={Link} />
      </head>
      <body>
        <DsfrProvider lang={lang}>
          <CustomHeader />
          <main className="fr-container">
            <Suspense>{children}</Suspense>
          </main>
          <CustomFooter />
        </DsfrProvider>
      </body>
    </html>
  );
}
