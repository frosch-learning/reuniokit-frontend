"use client";

// Types
import { FormData } from "@/src/types/form";

// Context
import { formDataContext, stepContext } from "@/src/contexts/contexts";

// Components
import Step1 from "@/src/components/Formulaire/StepOne/StepOne";
import Step2 from "@/src/components/Formulaire/StepTwo/StepTwo";
import Step3 from "@/src/components/Formulaire/StepThree/StepThree";
import Proposition from "@/src/components/Formulaire/Proposition/Proposition";

// lib
import { useState } from "react";

export default function DerouleFormulaire() {
  const [formValues, setFormValues] = useState<FormData>({
    sujet_reunion: "",
    sujet_detail: "",
    lieu: "",
    participants: "",
    duree: "",
    objectifs: {
      inclusion: [],
      corps_reunion: [],
      cloture: true,
    },
  });
  const [step, setStep] = useState(1);

  console.log("step global");
  console.log(step);
  console.log(formValues);

  const renderStep: any = () => {
    switch (step) {
      case 1:
        return <Step1 />;
      case 2:
        return <Step2 />;
      case 3:
        return <Step3 />;
      case 4:
        return <Proposition formValues={formValues} />;
    }
  };

  return (
    <>
      <style jsx global>{`
        * {
          margin: 0;
          padding: 0;
          box-sizing: border-box;
          text-decoration: none;
        }
      `}</style>
      <formDataContext.Provider value={{ formValues, setFormValues }}>
        <stepContext.Provider value={{ step, setStep }}>
          {renderStep()}
        </stepContext.Provider>
      </formDataContext.Provider>
    </>
  );
}
