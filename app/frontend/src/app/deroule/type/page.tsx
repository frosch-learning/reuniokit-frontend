"use client";

// Types
import { DerouleTypeSynthese } from "@/src/types/deroule";

// Style
import styles from "./deroule_type.module.scss";

// Components

// Lib
import { useState, useEffect } from "react";
import Breadcrumb from "@codegouvfr/react-dsfr/Breadcrumb";
import CardDerouleType from "@/src/components/Cards/CardDerouleType/CardDerouleType";

const book_icon = `${process.env.NEXT_PUBLIC_ASSETS}/reunion_type/img/clipboard.svg`;

export default function DerouleType() {
  const [deroulesTypesSyntheses, setDeroulesTypesSyntheses] =
    useState<DerouleTypeSynthese[]>();

  const fetchFicheData = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/api/v1/deroule/type/synthese`,
      );
      if (!response.ok) {
        throw new Error("Failed to fetch fiche data");
      }
      const data = await response.json();
      setDeroulesTypesSyntheses(data.results);
    } catch (error) {
      console.error("Error fetching fiche data:", error);
    }
  };

  useEffect(() => {
    fetchFicheData();
  }, []);

  if (!deroulesTypesSyntheses) {
    return <div>Récupération des données en cours.</div>;
  }

  return (
    <>
      <Breadcrumb
        currentPageLabel="Déroulés types"
        homeLinkProps={{
          href: `${process.env.NEXT_PUBLIC_DEPLOIEMENT}/`,
        }}
        segments={[]}
      />
      <div className={styles["container"]}>
        <main>
          <h1>
            {" "}
            <img src={book_icon} alt="clipboard" />
            Déroulés de réunion "types"
          </h1>
          <hr />

          <div className={styles["main-container-card"]}>
            {deroulesTypesSyntheses.map((derouleTypeSynthese, index) => (
              <CardDerouleType
                key={index}
                derouleTypeSynthese={derouleTypeSynthese}
              />
            ))}
          </div>
        </main>
      </div>
    </>
  );
}
