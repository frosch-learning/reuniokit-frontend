"use client";

// Types
import { DerouleType, DerouleTypeComplement } from "@/src/types/deroule";

// Style
import styles from "./deroule-type-card-details.module.scss";

// Constantes
import { hrefBibliotheque } from "@/src/constants/href";

// Lib
import React, { useEffect, useState } from "react";
import { useSearchParams } from "next/navigation";
import Link from "next/link";
import Breadcrumb from "@codegouvfr/react-dsfr/Breadcrumb";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import Tag from "@codegouvfr/react-dsfr/Tag";

// Components
import CardDerouleCustom from "@/src/components/Cards/CardDerouleCustom/CardDerouleCustom";

const clipboard = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/clipboard.svg`;
const computer = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/computer.svg`;
const timer = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/timer.svg`;
const team = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/team.svg`;
const barre = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/barre.svg`;
const download = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/download.svg`;
const information = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/information.svg`;
const book = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/book.svg`;
const barre_grande = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/barre_grande.svg`;
const marteau = `${process.env.NEXT_PUBLIC_ASSETS}/reunion_type/img/marteau.svg`;

const generatePdf = (filename: string) => {
  const input = document.getElementById("content");

  if (input) {
    html2canvas(input, { scale: 2 }).then((canvas) => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new jsPDF("p", "mm", "a4");
      const margin = 2;

      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = pdf.internal.pageSize.getHeight();

      const imgWidth = canvas.width * 0.264583;
      const imgHeight = canvas.height * 0.264583;

      const ratio = Math.min(
        (pdfWidth - 2 * margin) / imgWidth,
        (pdfHeight - 2 * margin) / imgHeight,
      );

      const canvasWidth = imgWidth * ratio;
      const canvasHeight = imgHeight * ratio;

      const x = margin;
      const y = margin;

      pdf.addImage(imgData, "PNG", x, y, canvasWidth, canvasHeight);
      pdf.save(filename + ".pdf");
    });
  }
};

function listDictToList(
  keyToSwap: string,
  listDict: Array<DerouleTypeComplement>,
): Array<DerouleTypeComplement> {
  let results: DerouleTypeComplement[] = [];

  listDict.forEach((element) => {
    if (element.categorie === keyToSwap) {
      results.push(element);
    }
  });

  return results;
}

function renderEtape(deroule: DerouleTypeComplement) {
  return (
    <>
      <p>{deroule.contenu}</p>
      {deroule.precisions
        ? renderDeroulementPrecisions(deroule.precisions)
        : null}
      {deroule.exemples ? renderDeroulementExemples(deroule.exemples) : null}
      {deroule.fiche_associee ? (
        <CardDerouleCustom
          detailSingleFiche={[deroule.fiche_associee]}
          multipleFiches={false}
        />
      ) : null}
    </>
  );
}

function renderDeroulementPrecisions(precisions: string) {
  if (!precisions) return null;
  const precisionsList = precisions.replace(/[\[\]]/g, "").split(",");
  return (
    <>
      <ul>
        {precisionsList.map((precision, index) => (
          <li key={index}>{precision}</li>
        ))}
      </ul>
    </>
  );
}

function renderDeroulementExemples(exemples: string) {
  if (!exemples) return null;
  const exemplesList = exemples.replace(/[\[\]]/g, "").split(",");
  return (
    <>
      <p>Par exemples:</p>
      <ul>
        {exemplesList.map((exemple, index) => (
          <li key={index}>{exemple}</li>
        ))}
      </ul>
    </>
  );
}

export default function DerouleTypeDetail() {
  const searchParams = useSearchParams();
  const derouleTypeId = searchParams.get("derouleType");
  const [materiels, setMateriels] = useState<DerouleTypeComplement[]>();
  const [deroulements, setDeroulements] = useState<DerouleTypeComplement[]>();
  const [derouleType, setDerouleType] = useState<DerouleType>();
  const [inclusion, setInclusion] = useState<DerouleTypeComplement>();
  const [corpsReunion, setCorpsReunion] = useState<DerouleTypeComplement[]>();
  const [cloture, setCloture] = useState<DerouleTypeComplement>();

  const fetchDerouleType = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/api/v1/deroule/${derouleTypeId}`,
      );
      if (!response.ok) {
        throw new Error("Failed to fetch fiche data");
      }
      const data = await response.json();
      setDerouleType(data.result);
    } catch (error) {
      console.error("Error fetching fiche data:", error);
    }
  };

  const fetchDerouleTypeComplement = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API_URL}/api/v1/deroule_complement/?q=(filters:!((col:id_deroule_type,opr:eq,value:${derouleTypeId})))`,
      );
      if (!response.ok) {
        throw new Error("Failed to fetch fiche data");
      }
      const data = await response.json();
      console.log(data.result);
      setMateriels(listDictToList("materiel", data.result));
      setDeroulements(listDictToList("deroulement", data.result));
    } catch (error) {
      console.error("Error fetching fiche data:", error);
    }
  };

  useEffect(() => {
    fetchDerouleType();
    fetchDerouleTypeComplement();
  }, []);

  useEffect(() => {
    if (derouleType && materiels && deroulements) {
      setInclusion(deroulements[0]);
      setCorpsReunion(deroulements.slice(1, -1));
      setCloture(deroulements.slice(-1)[0]);
      console.log(deroulements);
    }
  }, [derouleType, materiels, deroulements]);

  if (!derouleType) {
    return <div>Récupération des données</div>;
  }

  if (!inclusion || !corpsReunion || !cloture) {
    return <div>Récupération du déroulé</div>;
  }

  return (
    <>
      <Breadcrumb
        currentPageLabel="Page Actuelle"
        homeLinkProps={{
          href: "/",
        }}
        segments={[
          {
            label: "Déroulés types",
            linkProps: {
              href: "/deroule/type",
            },
          },
        ]}
      />
      <div id="content" className={styles["container"]}>
        <section>
          <div className={styles["header"]}>
            <div className={styles["header-left"]}>
              <p className={styles["header-left__page_title"]}>
                <img src={clipboard} alt="clipboard" />
                Déroulé type
              </p>
              <h2 className={styles["header-left__deroule_title"]}>
                {derouleType.nom}
              </h2>
              <hr />
              <div className={styles["header-left__informations"]}>
                <Tag className={styles["tag"]}>
                  <img src={computer} alt="computer" />
                  {derouleType.compatible_visio ? (
                    <>Distanciel / Présentiel</>
                  ) : (
                    <>Présentiel</>
                  )}
                </Tag>
                <Tag className={styles["tag"]}>
                  <img src={timer} alt="timer" />2 heures
                </Tag>
                <Tag className={styles["tag"]}>
                  <img src={team} alt="team" />
                  {derouleType.participant_max} personnes
                </Tag>
              </div>
              <div className={styles["header-left__deroule_description"]}>
                {derouleType.description_longue}
              </div>
            </div>
            <div className={styles["header-right"]}>
              <div className={styles["header-right__materiel"]}>
                <p>
                  <img src={marteau} alt="Marteau" /> Materiel :
                </p>
                <ul className={styles["presentation-right-materiel__option"]}>
                  {materiels!.map((materiel, index) => (
                    <li key={index}>{materiel.contenu}</li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </section>
        <hr />
        <section className={styles["deroule"]}>
          <h3>Déroulé</h3>

          {/* INCLUSION */}
          <div className={styles["deroule-inclusion"]}>
            <div className={styles["deroule-inclusion-title"]}>
              <h4>Inclusion</h4>
              <img
                src={barre}
                alt="barre verticale"
                className={styles["deroule-inclusion-title__barre"]}
              />
              <p className={styles["deroule-inclusion-title__time"]}>
                <img src={timer} alt="timer" />
                15 mn
              </p>
            </div>
            <div className={styles["etape"]}>{renderEtape(inclusion)}</div>
          </div>

          <hr />
          {/* CORPS DE LA REUNION */}
          <div className={styles["deroule-corps"]}>
            <div className={styles["deroule-inclusion-title"]}>
              <h4>Corps de la réunion</h4>
              <img
                src={barre}
                alt="barre verticale"
                className={styles["deroule-inclusion-title__barre"]}
              />
              <p className={styles["deroule-inclusion-title__time"]}>
                <img src={timer} alt="timer" />
                25 mn
              </p>
            </div>

            {corpsReunion.map((etape, index) => (
              <div key={index} className={styles["etape"]}>
                {renderEtape(etape)}
              </div>
            ))}
          </div>
          <hr />

          {/* CLOTURE */}
          <div className={styles["deroule-cloture"]}>
            <div className={styles["deroule-inclusion-title"]}>
              <h4>Clôture</h4>
              <img
                src={barre}
                alt="barre verticale"
                className={styles["deroule-inclusion-title__barre"]}
              />
              <p className={styles["deroule-inclusion-title__time"]}>
                <img src={timer} alt="timer" />
                15 mn
              </p>
            </div>
            {renderEtape(cloture)}
          </div>
        </section>
        <section className={styles["telechargement"]}>
          <div className={styles["telechargement-left"]}>
            <div className={styles["telechargement-left-top"]}>
              <button
                className={styles["telechargement-left-top__download"]}
                onClick={() => generatePdf(derouleType.nom)}
              >
                {" "}
                <img src={download} alt="télécharger" /> Télécharger le déroulé
                type
              </button>
              <div className={styles["telechargement-left-top__description"]}>
                <p>Pour en savoir plus :</p>
                <ul>
                  <li>Titre article </li>
                  <li>Titre article </li>
                </ul>
              </div>
            </div>
            <div className={styles["telechargement-left-bottom"]}>
              <p>
                <img src={information} alt="information" />1 MB, PDF
              </p>
            </div>
          </div>
          <img src={barre_grande} alt="grande barre" />
          <div className={styles["telechargement-right"]}>
            <div className={styles["telechargement-right-card"]}>
              <p>
                Retrouver d’autres options de séquences dans la biblitohèque
              </p>
              <button>
                <Link href={hrefBibliotheque}>
                  <img src={book} alt="livre" />
                  Bibliothèque
                </Link>
              </button>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
