"use client";

// Types & constants
import {
  hrefHome,
  hrefConnexion,
  hrefDerouleFormulaire,
  hrefDerouleType,
  hrefBibliotheque,
  hrefFaq,
} from "@/src/constants/href";

// Style
import styles from "./home.module.scss";

// Components
import CardPresentationLink from "@/src/components/Cards/CardPresentationLink/CardPresentationLink";
import CardAutresRessources from "@/src/components/Cards/CardAutresRessources/CardAutresRessources";
import Button from "@/src/components/Buttons/Button/Button";

// Lib
import React from "react";

const ReunioKit = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/ReunioKit.svg`;
const generator_icon = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/icon_generator.svg`;
const deroule_icon = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/icon_deroule.svg`;
const book_icon = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/icon_library.svg`;
const icon_button_deroule = `${process.env.NEXT_PUBLIC_ASSETS}/button/img/icon_button_deroule.svg`;
const icon_button_book = `${process.env.NEXT_PUBLIC_ASSETS}/button/img/icon_button_book.svg`;
const deroule_type_frame1 = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/deroule_type_frame1.svg`;
const deroule_type_frame2 = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/deroule_type_frame2.svg`;
const deroule_type_frame3 = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/deroule_type_frame3.svg`;
const rectangle = `${process.env.NEXT_PUBLIC_ASSETS}/home/img/rectangle.svg`;

export default function Home() {
  return (
    <>
      <div className={styles["home-container"]}>
        <div className={styles["img-container"]}>
          <div className={styles["img-container-left"]}>
            <img
              className={styles["middle_container__img__title"]}
              src={ReunioKit}
              alt="logo middle_container reuniokit"
            />
            <img
              src={
                process.env.NEXT_PUBLIC_ASSETS + "/home/img/bercylab__logo.svg"
              }
              alt="bercyLab"
            />
          </div>
        </div>
        <div className={styles["middle_container__group"]}>
          <p>
            Concevez des réunions efficaces, adaptées, sur mesure, à composer à
            partir de ressources ou de modèles “type”.
          </p>
          <div className={styles["middle_container__group_card"]}>
            <CardPresentationLink
              icon={generator_icon}
              buttonIcon={icon_button_deroule}
              text="Mon déroulé sur-mesure"
              buttonLabel="Générateur"
              onButtonClick={() => {}}
              mainColor="#CA84F5"
              href={hrefDerouleFormulaire}
            />
            <CardPresentationLink
              icon={deroule_icon}
              buttonIcon={icon_button_book}
              text="Les déroulés types de réunion"
              buttonLabel="Déroulés types"
              onButtonClick={() => {}}
              mainColor="#8585F6"
              href={hrefDerouleType}
            />
            <CardPresentationLink
              icon={book_icon}
              buttonIcon={icon_button_book}
              text="Bibliothèque de séquences d’animation"
              buttonLabel="Bibliothèque"
              onButtonClick={() => {}}
              mainColor="#DE7899"
              href={hrefBibliotheque}
            />
          </div>
          <div className={styles["middle_container__group_text"]}>
            <p>Concevoir un déroulé qui corresponde à mes objectifs</p>
            <p>Découvrir des déroulés clés en main</p>
            <p>S'inspirer en découvrant de nouvelles fiches méthodes</p>
          </div>
        </div>
        <section className={styles["section-1"]}>
          <h2>Qu'est-ce que c'est</h2>
          <p>
            Cadre général : espace, temps, objectifs, séquences, ordre du jour
          </p>
          <a href={hrefFaq}>→ En savoir plus</a>
        </section>
        <section className={styles["section-2"]}>
          <h2>
            {" "}
            <img src={generator_icon} alt="" />
            Le générateur
          </h2>
          <p>
            Vous avez besoin de créer un déroulé personnalisé à vos besoins ?
            Indiquez simplement le sujet de votre réunion, vos objectifs, le
            lieu, le nombre de participants, la durée souhaitée dans notre
            générateur. Il vous proposera un déroulé complet !
          </p>
          <p>→ Réaliser votre déroulé de réunion sur-mesure ici :</p>
          <div className={styles["button-container"]}>
            <Button
              buttonColor="#4D1C6B"
              icon={icon_button_deroule}
              text="Générateur"
              href={hrefDerouleFormulaire}
              buttonType={"primary"}
            />
          </div>
        </section>
        <section className={styles["section-3"]}>
          <h2>
            {" "}
            <img src={deroule_icon} alt="" />
            Les déroulés types de réunion
          </h2>
          <p>
            Vous souhaitez consulter des déroulés de réunion conçus de A à Z ?
            Nous avons élaboré pour vous des déroulés détaillés couvrant divers
            aspects des réunions : la vie et l'organisation de votre service, la
            conduite de projets et des problématiques récurrentes
          </p>
          <div className={styles["card-container-deroule"]}>
            <img src={deroule_type_frame1} alt="" />
            <img src={deroule_type_frame2} alt="" />
            <img src={deroule_type_frame3} alt="" />
          </div>
          <div className={styles["card-container-text"]}>
            <p>Réunion créative avec mon équipe.</p>
            <p>Réunion en ligne pour établie une feuille de route.</p>
            <p>Réunion de service et introduction de nouveau collaborateurs.</p>
          </div>
          <p>→ Retrouver des déroulés types ici : </p>

          <div className={styles["button-container"]}>
            <Button
              buttonColor="#000091"
              icon={icon_button_book}
              text="Déroulés types"
              href={hrefDerouleType}
              buttonType={"primary"}
            />
          </div>
        </section>
        <section className={styles["section-4"]}>
          <h2>
            {" "}
            <img src={book_icon} alt="" />
            La biblitohèque de fiches méthodes
          </h2>
          <p>
            Vous souhaitez consulter des méthodes d’intelligence collective ?
            Vous pouvez consultez notre bibliothèque de fiches méthodes pour
            vous inspirer, et animer vos réunions en présentiel comme en
            distanciel.
          </p>
          <p>→ Réaliser votre déroulé de réunion sur-mesure ici :</p>
          <div className={styles["button-container"]}>
            <Button
              buttonColor="#AB214E"
              icon={icon_button_book}
              text="Bibliothèque"
              href={hrefBibliotheque}
              buttonType={"primary"}
            />
          </div>
        </section>
        <section className={styles["section-5"]}>
          <h2>D'autres ressources de l’administration</h2>
          <div className={styles["card-container-partner"]}>
            <CardAutresRessources
              titre="Utilo"
              contenu="Le Ti Lab a accompagné sa communauté d’innovateur d’intérêt général dans la conception coopérative d’une boîte à outils partagée de pratiques innovantes et collaboratives."
              partenaire="Utilo"
              url="https://www.utilo.org/"
            />
            <CardAutresRessources
              titre="Guide de facilitation à distance"
              contenu="L'équipe innovation de la Direction interministérielle de la transformation publique a réalisé un guide de facilitation en ligne. L'objectif : donner aux facilitateurs, débutants ou confirmés, des repères pour mieux concevoir et gérer des ateliers de travail collaboratif à distance."
              partenaire="DITP"
              url="https://www.modernisation.gouv.fr/outils-et-formations/guide-de-la-facilitation-distance"
            />
          </div>
        </section>
        <section className={styles["section-6"]}>
          <h2>La Mission Innovation vous propose</h2>
          <a
            className={styles["main_link"]}
            href="https://www.economie.gouv.fr/mission-innovation/les-parcours-apprenants"
            target="_blank"
          >
            lesparcoursapprenants.fr
          </a>
          <div className={styles["card-container-title"]}>
            <p>Disco Pass</p>
            <p>Pass Innov’</p>
            <p>Facilit'àcoeurs</p>
          </div>
          <div className={styles["card-container-text"]}>
            <p>
              Ut aedificio, ut vestitu cultuque corporis, animante virtute
              praedito, eo qui vel amare
            </p>
            <p>
              Ut aedificio, ut vestitu cultuque corporis, animante virtute
              praedito, eo qui vel amare
            </p>
            <p>
              Ut aedificio, ut vestitu cultuque corporis, animante virtute
              praedito, eo qui vel amare
            </p>
          </div>
          <div className={styles["card-container-mi"]}>
            <img src={rectangle} alt="" />
            <img src={rectangle} alt="" />
            <img src={rectangle} alt="" />
          </div>
          <div className={styles["card-container-link"]}>
            <div className={styles["link-container"]}>
              <a
                href="https://www.economie.gouv.fr/mission-innovation/les-parcours-apprenants/discopass"
                target="_blank"
              >
                discopass.fr
              </a>
            </div>
            <div className={styles["link-container"]}>
              <a
                href="https://www.economie.gouv.fr/mission-innovation/les-parcours-apprenants/discopass"
                target="_blank"
              >
                systeme-de-design.gouv.fr
              </a>
            </div>
            <div className={styles["link-container"]}>
              <a
                href="https://www.economie.gouv.fr/mission-innovation/les-parcours-apprenants/discopass"
                target="_blank"
              >
                systeme-de-design.gouv.fr
              </a>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
