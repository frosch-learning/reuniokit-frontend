"use client";

// libs
import React from "react";
import { Footer } from "@codegouvfr/react-dsfr/Footer";

const CustomFooter = () => {
  return (
    <Footer
      brandTop={
        <>
          Ministère
          <br />
          de l'Économie
          <br />
          des Finances
          <br />
          et de la Souveraineté
          <br />
          industrielle et numérique
        </>
      }
      accessibility="fully compliant"
      contentDescription="
    Si vous souhaitez aller plus loin, vous pouvez trouver des informations complémentaires en naviguant sur les sites ci-dessous
"
      license={
        <>
          Sauf mention contraire, tous les contenus de ce site sont sous {""}
          <a
            href="https://github.com/etalab/licence-ouverte/blob/master/LO.md"
            target="_blank"
          >
            licence etalab-2.0
          </a>{" "}
        </>
      }
    />
  );
};

export default CustomFooter;
