"use client";

// Types and constants
import {
  hrefHome,
  hrefConnexion,
  hrefDerouleFormulaire,
  hrefDerouleType,
  hrefBibliotheque,
  hrefFaq,
} from "@/src/constants/href";

// libs
import React from "react";
import { Header } from "@codegouvfr/react-dsfr/Header";
import { Badge } from "@codegouvfr/react-dsfr/Badge";

const CustomHeader = () => {
  return (
    <Header
      brandTop={
        <>
          Ministère
          <br />
          de l'Économie
          <br />
          des Finances
          <br />
          et de la Souveraineté
          <br />
          industrielle et numérique
        </>
      }
      homeLinkProps={{
        href: hrefHome,
        title:
          "Accueil Réunio'kit - Ministère de l'Économie des Finances et de la Souveraineté industrielle et numérique",
      }}
      id="fr-header-header-with-quick-access-items"
      quickAccessItems={[
        {
          iconId: "fr-icon-drag-move-2-line",
          linkProps: { href: hrefDerouleFormulaire },
          text: "Générateur",
        },
        {
          iconId: "fr-icon-clipboard-line",
          linkProps: { href: hrefDerouleType },
          text: "Déroulés types",
        },
        {
          iconId: "fr-icon-booklet-line",
          linkProps: { href: hrefBibliotheque },
          text: "Bibliothèque",
        },
        {
          iconId: "fr-icon-question-line",
          linkProps: { href: hrefFaq },
          text: "FAQ",
        },
        {
          iconId: "fr-icon-account-circle-line",
          linkProps: { href: hrefConnexion },
          text: "Espace administrateur",
        },
      ]}
      serviceTagline="Un outil contre la réunionite"
      serviceTitle={
        <>
          Réunio'Kit{" "}
          <Badge as="span" noIcon severity="success">
            Beta
          </Badge>
        </>
      }
    />
  );
};

export default CustomHeader;
