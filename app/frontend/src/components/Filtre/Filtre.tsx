"use client";

// Types
import { FiltreProps } from "@/src/types/filtre";

// Style
import "./filtre.scss";

// Libs
import React from "react";

const Filtre: React.FC<FiltreProps> = ({ filterGroups, onFilterChange }) => {
  if (!filterGroups.length) return null;

  return (
    <div className="filtre-container">
      {filterGroups.map((group, groupIndex) => (
        <div key={group.label} className="filtre-group">
          <select onChange={(e) => onFilterChange(e.target.value, groupIndex)}>
            <option value="">{group.label}</option>
            {group.options.map((option) => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </select>
        </div>
      ))}
    </div>
  );
};

export default Filtre;
