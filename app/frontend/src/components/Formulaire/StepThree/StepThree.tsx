"use client";

// Context
import { formDataContext, stepContext } from "@/src/contexts/contexts";

// Types
import { Objectifs } from "@/src/types/fiche";

// Style
import styles from "./StepThree.module.scss";

// Components
import Button from "@/src/components/Buttons/Button/Button";

// Lib
import React, { useState, useEffect, useContext } from "react";

const menGroup__img =
  process.env.NEXT_PUBLIC_ASSETS + "/algo/img/pc_screen.svg";
const levelBarCompleted__img =
  process.env.NEXT_PUBLIC_ASSETS + "/algo/img/levelbar__completed.svg";
const levelBarNot__img =
  process.env.NEXT_PUBLIC_ASSETS + "/algo/img/levelbar__not.svg";
const back_icon = process.env.NEXT_PUBLIC_ASSETS + "/algo/img/arrow-back.svg";

const Step3 = () => {
  // Context
  const { formValues, setFormValues } = useContext(formDataContext);
  const { step, setStep } = useContext(stepContext);

  // Stocker les objectifs possibles
  const [objectifsPossibles, setObjectifsPossibles] = useState<Objectifs>();

  const handleNextStep = (sens: string) => {
    if (sens === "prev") {
      setStep(step - 1);
    }
    if (sens === "next") {
      setStep(step + 1);
    }
  };

  const getObjectifs = (sujet_reunion: string, sujet_detail: string) => {
    try {
      fetch(process.env.NEXT_PUBLIC_API_URL + "/api/v1/deroule/objectifs", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          sujet_reunion: sujet_reunion,
          sujet_detail: sujet_detail,
        }),
      })
        .then((res) => res.json())
        .then((jsonData) => {
          setObjectifsPossibles(jsonData);
        });
    } catch (error) {
      console.error("Erreur lors de la requête à l'API :", error);
    }
  };

  const isSelected = (objectif: string) => {
    if (!formValues.objectifs?.corps_reunion) {
      return false;
    }
    return formValues.objectifs.corps_reunion.includes(objectif);
  };

  const handleObjectifsListe = (
    objectifsSelectionnees: string[],
    objectifSupplementaire: string,
  ) => {
    if (!objectifsSelectionnees) {
      return [objectifSupplementaire];
    }
    if (objectifsSelectionnees.includes(objectifSupplementaire)) {
      return objectifsSelectionnees.filter(
        (value: string) => value !== objectifSupplementaire,
      );
    } else {
      return [...objectifsSelectionnees, objectifSupplementaire];
    }
  };

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    if (name === "inclusion") {
      setFormValues({
        ...formValues,
        objectifs: {
          ...formValues.objectifs,
          inclusion: [value],
        },
      });
    }
    if (name === "cloture") {
      setFormValues({
        ...formValues,
        [name]: value,
      });
    }
    if (name === "corps_reunion") {
      setFormValues({
        ...formValues,
        objectifs: {
          ...formValues.objectifs,
          [name]: handleObjectifsListe(formValues.objectifs[name], value),
        },
      });
    }
  };

  useEffect(() => {
    // Vérifiez si formValues.objectifs existe, sinon, initialisez-le
    if (!formValues.objectifs) {
      setFormValues({
        ...formValues,
        objectifs: {
          inclusion: [],
          corps_reunion: [],
          cloture: true,
        },
      });
    } else {
      // Si objectifs existe, mettre à jour la valeur de cloture
      setFormValues({
        ...formValues,
        objectifs: {
          ...formValues.objectifs,
          cloture: true,
        },
      });
    }

    // Récupérer les objectifs
    getObjectifs(formValues.sujet_reunion, formValues.sujet_detail);
  }, []);

  return (
    <div className={styles["globalContainer"]}>
      <main>
        <div className={styles["main__container"]}>
          <div className={styles["main__container__left"]}>
            <h1>Créer mon déroulé de réunion sur-mesure</h1>
            <p>DÉFINIR MON DÉROULÉ / MES SÉQUENCES</p>
            <div className={styles["main__container__image"]}>
              <img
                src={menGroup__img}
                alt="Illustration d'un groupe de personnes"
              />
            </div>
          </div>

          <div className={styles["main__container__option"]}>
            <p>Étape 3 sur 3</p>
            <div className={styles["levelBar"]}>
              <img src={levelBarCompleted__img} alt="Étape terminée" />
              <img src={levelBarCompleted__img} alt="Étape terminée" />
              <img src={levelBarCompleted__img} alt="Étape terminée" />
            </div>
            <p className={styles["txt_explication"]}>
              Le déroulé de l'atelier est proposé en 3 temps. Les temps
              d'inclusion et de clôture sont préconisés en fonction de votre
              contexte.
            </p>
            {objectifsPossibles ? (
              <>
                <fieldset>
                  <legend>1 | Inclusion</legend>
                  <div className={styles["inclusion"]}>1 | Inclusion</div>
                  <p className={styles["temps_inclusion"]}>Temps d’inclusion</p>
                  <div className={styles["radioGroup"]}>
                    {objectifsPossibles["inclusion"].map(
                      (inclusion: string, index: number) => (
                        <div key={index} className={styles["radioItem"]}>
                          <input
                            type="radio"
                            id={inclusion}
                            name="inclusion"
                            value={inclusion}
                            checked={
                              formValues.objectifs?.inclusion?.includes(
                                inclusion,
                              ) || false
                            }
                            onChange={handleInputChange}
                          />
                          <label htmlFor={inclusion}>{inclusion}</label>
                        </div>
                      ),
                    )}
                  </div>
                </fieldset>
                <fieldset>
                  <legend>2 | Corps de la réunion</legend>
                  <div className={styles["inclusion"]}>
                    2 | Corps de la réunion
                  </div>
                  <p className={styles["temps_inclusion"]}>
                    Sélectionner un ou plusieurs objectifs. Vous n’arrivez pas à
                    déterminer vos objectifs ?{" "}
                    <span>Retrouver une aide ici.</span>
                  </p>
                  <div className={styles["checkboxGroup"]}>
                    {objectifsPossibles["corps_reunion"].map(
                      (corps_reunion: string, index: number) => (
                        <div key={index} className={styles["checkboxItem"]}>
                          <input
                            type="checkbox"
                            id={corps_reunion}
                            name="corps_reunion"
                            value={corps_reunion}
                            checked={isSelected(corps_reunion)}
                            onChange={handleInputChange}
                          />
                          <label htmlFor={corps_reunion}>{corps_reunion}</label>
                        </div>
                      ),
                    )}
                  </div>
                </fieldset>
                <div className={styles["inclusion"]}>3 | Clôture</div>
                <p className={styles["temps_inclusion"]}>
                  Un moment de clôture est nécessaire
                </p>
                <div className={styles["buttonContainer"]}>
                  <Button
                    icon={back_icon}
                    className={styles["buttonBack"]}
                    text="Revenir à l'étape précédente"
                    onClick={() => handleNextStep("prev")}
                    buttonType="primary"
                  ></Button>
                  <Button
                    text="Valider et continuer"
                    className={styles["buttonNext"]}
                    onClick={() => handleNextStep("next")}
                    buttonType="primary"
                  ></Button>
                </div>
              </>
            ) : (
              <div>Chargement des objectifs</div>
            )}
          </div>
        </div>
      </main>
    </div>
  );
};

export default Step3;
