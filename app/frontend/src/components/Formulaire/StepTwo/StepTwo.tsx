"use client";

// Context
import { formDataContext, stepContext } from "@/src/contexts/contexts";

// Style
import styles from "./StepTwo.module.scss";

// Components
import Button from "../../Buttons/Button/Button";

// Lib
import React, { useState, useContext } from "react";
import { Select } from "@codegouvfr/react-dsfr/Select";

const robot__img = process.env.NEXT_PUBLIC_ASSETS + "/algo/img/planche.svg";
const back_icon = process.env.NEXT_PUBLIC_ASSETS + "/algo/img/arrow-back.svg";

const Step2 = () => {
  const { formValues, setFormValues } = useContext(formDataContext);
  const { step, setStep } = useContext(stepContext);

  const lieux = ["Présentiel", "Distanciel"];

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setFormValues({
      ...formValues,
      [name]: value,
    });
  };

  const handleNextStep = (sens: string) => {
    if (sens === "prev") {
      setStep(step - 1);
    }
    if (sens === "next") {
      setStep(step + 1);
    }
  };

  return (
    <div className={styles.globalContainer}>
      <main>
        <div className={styles.mainContainer}>
          <div className={styles.mainContainerLeft}>
            <h1>Créer mon déroulé de réunion sur-mesure</h1>
            <p>DÉFINIR LES CONTRAINTES DE LA RÉUNION</p>
            <div className={styles.mainContainerImage}>
              <img src={robot__img} alt="robot" />
            </div>
          </div>

          <div className={styles.mainContainerOption}>
            <p>Étape 2 sur 3</p>
            <div className={styles.levelBar}>
              <img
                src={
                  process.env.NEXT_PUBLIC_ASSETS +
                  "/algo/img/levelbar__completed.svg"
                }
                alt=""
              />
              <img
                src={
                  process.env.NEXT_PUBLIC_ASSETS +
                  "/algo/img/levelbar__completed.svg"
                }
                alt=""
              />
              <img
                src={
                  process.env.NEXT_PUBLIC_ASSETS + "/algo/img/levelbar__not.svg"
                }
                alt=""
              />
            </div>

            <fieldset className={styles.hiddenFieldset}>
              <legend className={styles.hiddenLegend}>1 | Lieu</legend>
              <h2>1 | Lieu</h2>
              <hr />
              <div className={styles.radioGroup}>
                {lieux.map((lieu, index) => (
                  <div key={lieu} className={styles.radioItem}>
                    <input
                      type="radio"
                      id={lieu}
                      name="lieu"
                      value={lieu}
                      checked={formValues.lieu === lieu}
                      onChange={(e) => {
                        handleInputChange(e);
                      }}
                    />
                    <label htmlFor={lieu}>{lieux[index]}</label>
                  </div>
                ))}
              </div>
              <div className={styles.radioGroupTxt}>
                Pour l'organisation d'atelier en mode hybride, il est conseillé
                de privilégier la participation à distance pour tous les
                intervenants.
              </div>
            </fieldset>

            <h2>2 | Nombre de participants approximatifs</h2>
            <hr />
            <input
              type="number"
              className={styles.nbParticipant}
              name="participants"
              placeholder="Nombre de participants"
              value={formValues.participants}
              onChange={(e) => handleInputChange(e)}
            />
            <p className={styles.nbPersonnes}>
              Nous recommandons 4 à 30 participants. Dans le cas où vous
              organisez un atelier avec de nombreux participants, nous vous
              conseillons de contacter le BercyLab à{" "}
              <a href="mailto:bercylab@finances.gouv.fr">
                bercylab@finances.gouv.fr
              </a>{" "}
              pour vous accompagner ou vous former !
            </p>

            <h2>3 | Durée de la réunion</h2>
            <hr />
            <Select
              label={null}
              nativeSelectProps={{
                onChange: (e) => handleInputChange(e),
                name: "duree",
              }}
            >
              <option value="0" selected disabled hidden>
                Selectionnez une option
              </option>
              <option value="60">1h00</option>
              <option value="90">1h30</option>
              <option value="120">2h00</option>
              <option value="150">2h30</option>
              <option value="180">3h00</option>
              <option value="210">3h30</option>
              <option value="240">4h00</option>
              <option value="0">Je ne sais pas / Ne pas renseigner</option>
            </Select>

            <div className={styles.buttonContainer}>
              <Button
                icon={back_icon}
                className={styles.buttonBack}
                text="Revenir à l'étape précédente"
                onClick={() => handleNextStep("prev")}
                buttonType="primary"
              ></Button>
              <Button
                text="Valider et continuer"
                className={styles.buttonNext}
                onClick={() => handleNextStep("next")}
                buttonType="primary"
              ></Button>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Step2;
