// Style
import styles from "./deroule-type-card.module.scss";

// Lib
import React from "react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { DerouleTypeSynthese } from "@/src/types/deroule";

const timer = `${process.env.NEXT_PUBLIC_ASSETS}/reunion_type/img/timer.svg`;
const map_pin = `${process.env.NEXT_PUBLIC_ASSETS}/reunion_type/img/map_pin.svg`;
const team = `${process.env.NEXT_PUBLIC_ASSETS}/reunion_type/img/team.svg`;
const illustration_reunion = `${process.env.NEXT_PUBLIC_ASSETS}/reunion_type/img/illustration_reunion.jpg`;
const eye = `${process.env.NEXT_PUBLIC_ASSETS}/reunion_type/img/eye.svg`;
const barre = `${process.env.NEXT_PUBLIC_ASSETS}/reunion_type/img/barre.svg`;

function disponibleEnDistanciel(compatible_visio: boolean) {
  return compatible_visio ? "Présentiel / Distanciel" : "Présentiel";
}

export default function CardDerouleType({
  derouleTypeSynthese,
}: {
  derouleTypeSynthese: DerouleTypeSynthese;
}) {
  const router = useRouter();
  // Redirection vers le déroulé type détaillée au clic
  const handleClick = (idDerouleType: string) => {
    router.push(
      process.env.NEXT_PUBLIC_DEPLOIEMENT +
        "/deroule/type/detail?derouleType=" +
        idDerouleType,
    );
  };

  return (
    <div className={styles["card_deroule_type"]}>
      <p className={styles["title"]}>{derouleTypeSynthese.nom}</p>
      <div className={styles["container_information_top"]}>
        <div className={styles["temps"]}>
          <img src={timer} alt="timer" />
          <p>{derouleTypeSynthese.duree_max} min</p>
        </div>
        <img src={barre} alt="barre verticale" />
        <div className={styles["format"]}>
          <img src={map_pin} alt="map pin" />
          <p>{disponibleEnDistanciel(derouleTypeSynthese.compatible_visio)}</p>
        </div>
        <img src={barre} alt="barre verticale" />
        <div className={styles["nombre_personne"]}>
          <img src={team} alt="team" />
          <p>
            {derouleTypeSynthese.participant_min} à{" "}
            {derouleTypeSynthese.participant_max} personnes
          </p>
        </div>
      </div>
      <img src={illustration_reunion} alt="illustration reunion" />
      <p className={styles["description"]}>
        {derouleTypeSynthese.description_longue}
      </p>
      <div className={styles["container_button"]}>
        <button onClick={() => handleClick(derouleTypeSynthese.id)}>
          <img src={eye} alt="eye" />
          Consulter le déroulé
        </button>
      </div>
    </div>
  );
}
