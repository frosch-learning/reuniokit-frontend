// Types
import { FicheSynthese } from "@/src/types/fiche";

// Style
import styles from "./cardDerouleCustom.module.scss";

// Lib
import React, { useState, useEffect } from "react";
import Badge from "@codegouvfr/react-dsfr/Badge";
import { useRouter } from "next/navigation";

// Constantes
const barre_rose = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/barre_rose.svg`;
const timer_rose = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/timer_rose.svg`;
const flashlight = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/flashlight.svg`;
const star_rose = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/star_rose.svg`;
const star_grise = `${process.env.NEXT_PUBLIC_ASSETS}/Cards/img/star_grise.svg`;
const arrow_left = `${process.env.NEXT_PUBLIC_ASSETS}/proposition/img/arrow_left.svg`;
const arrow_right = `${process.env.NEXT_PUBLIC_ASSETS}/proposition/img/arrow_right.svg`;

// Si on doit afficher uniquement une seule fiche sans autre possibilité
// on envoie liste de FicheSynthese avec 1 seul élément
export default function CardDerouleCustom({
  detailSingleFiche,
  multipleFiches,
  dureePhase,
  onDurationChange,
}: {
  detailSingleFiche: FicheSynthese[];
  multipleFiches: boolean;
  dureePhase?: number[];
  onDurationChange?: (duration: number[]) => void;
}) {
  const [fiche, setFiche] = useState<FicheSynthese>({
    id: 0,
    nom: "",
    objectif: "",
    duree_max: 0,
    difficulte: 1,
    description_courte: "",
  });
  const router = useRouter();
  const [currentIndexFiche, setCurrentIndexFiche] = useState(0);
  const [maxIndexFiche, setMaxIndexFiche] = useState(0);

  const handleClick = (idFiche: number) => {
    router.push(
      process.env.NEXT_PUBLIC_DEPLOIEMENT +
        "/bibliotheque/Fiche?fiche=" +
        idFiche,
    );
  };

  function incrementerIndex() {
    setCurrentIndexFiche(
      (prevIndex) => (prevIndex + 1) % detailSingleFiche.length,
    );
  }

  function decrementerIndex() {
    setCurrentIndexFiche((prevIndex) => {
      if (prevIndex === 0) {
        return detailSingleFiche.length - 1; // Retourne au dernier élément si l'index est déjà à 0
      } else {
        return prevIndex - 1;
      }
    });
  }

  function generateObjectifPart() {
    if (multipleFiches) {
      return (
        <div className={styles["objectif"]}>
          <div className={styles["objectif-title"]}>
            <p className={styles["number"]}>1</p>
            <p className={styles["texte"]}>Objectif</p>
          </div>
          <Badge className={styles["badge"]}>
            <img src={flashlight} alt="éclair" />
            {fiche.objectif}
          </Badge>
        </div>
      );
    }
    return;
  }

  function generateRightOtherFiches() {
    if (multipleFiches) {
      return (
        <div className={styles["other-fiche"]}>
          <p>
            {currentIndexFiche + 1} / {maxIndexFiche}
          </p>
          <div className={styles["other-fiche__button"]}>
            <button
              onClick={(event) => {
                event.preventDefault();
                decrementerIndex();
              }}
            >
              <img src={arrow_left} alt="fleche gauche" />
            </button>
            <button
              onClick={(event) => {
                event.preventDefault();
                incrementerIndex();
              }}
            >
              <img src={arrow_right} alt="fleche droite" />
            </button>
          </div>
          <p>Consulter d'autres fiches </p>
        </div>
      );
    }
    return;
  }

  useEffect(() => {
    if (detailSingleFiche && detailSingleFiche.length > 0) {
      setFiche(detailSingleFiche[currentIndexFiche]);
      setMaxIndexFiche(detailSingleFiche.length);

      if (onDurationChange && fiche && dureePhase) {
        // Trouver l'index de la première occurrence de la durée
        const duree = fiche.duree_max;
        const index = dureePhase.findIndex((element) => element === duree);
        console.log("INDEX -- " + index);
        // Remplacer la valeur si elle existe dans le tableau
        if (index !== -1) {
          console.log("FOUND");
          dureePhase[index] = duree;
          onDurationChange(dureePhase);
        } else {
          console.log("NOT FOUND");
          onDurationChange([duree]);
        }
      }
    } else {
      console.error("detailSingleFiche is undefined or empty");
    }
  }, [currentIndexFiche, fiche]);

  if (!fiche) {
    return <div>Fiche en cours de récupération...</div>;
  }

  return (
    <div className={styles["container"]}>
      {generateObjectifPart()}
      {/* CARD */}
      <div className={styles["card"]} onClick={() => handleClick(fiche.id)}>
        <div className={styles["card-header"]}>
          <p>{fiche.nom}</p>
          <img
            src={barre_rose}
            alt="barre verticale"
            className={styles["card-header__barre"]}
          />
          <p className={styles["minute"]}>
            <img src={timer_rose} alt="timer" />
            {fiche.duree_max} min
          </p>
          <img
            src={barre_rose}
            alt="barre verticale"
            className={styles["card-header__barre"]}
          />
          <p>
            Difficulté
            {Array(Number(fiche.difficulte))
              .fill(null)
              .map((_, i) => (
                <img key={i} src={star_rose} alt="étoile pleine" />
              ))}
            {Array(3 - Number(fiche.difficulte))
              .fill(null)
              .map((_, i) => (
                <img key={i} src={star_grise} alt="étoile vide" />
              ))}
          </p>
        </div>
        <p className={styles["card-content"]}>
          {fiche.description_courte.length > 200 ? (
            <>{fiche.description_courte.slice(0, 150) + "..."}</>
          ) : (
            <>{fiche.description_courte}</>
          )}
        </p>
      </div>
      {generateRightOtherFiches()}
    </div>
  );
}
