"use client";
// Types
import { FicheSynthese } from "@/src/types/fiche";

// Style
import styles from "./card-library.module.scss";

// Lib
import React from "react";
import { useRouter } from "next/navigation";

// Constantes
const star_full = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/star_full.svg`;
const star_empty = `${process.env.NEXT_PUBLIC_ASSETS}/fiche/img/star_empty.svg`;

export default function CardLibrary({
  detailSingleFiche,
}: {
  detailSingleFiche: FicheSynthese;
}) {
  const timer = process.env.NEXT_PUBLIC_ASSETS + "/list/img/timer.svg";
  const router = useRouter();

  // Redirection vers la fiche détaillée au clic
  const handleClick = (idFiche: number) => {
    router.push(
      process.env.NEXT_PUBLIC_DEPLOIEMENT +
        "/bibliotheque/Fiche?fiche=" +
        idFiche,
    );
  };

  // Si la description courte est nulle, on met une valeur par défaut
  const description =
    detailSingleFiche.description_courte ?? "Aucune description renseignée.";

  return (
    <div
      className={styles["card-container"]}
      onClick={() => handleClick(detailSingleFiche.id)} // Remplacer par handleClick directement
      aria-label={`Accéder au détail de la fiche ${detailSingleFiche.nom}`}
    >
      <p className={styles["card-container__objectif"]}>
        {detailSingleFiche.objectif}
      </p>
      <h3>{detailSingleFiche.nom}</h3>
      <p className={styles["card-container__text"]}>
        {description.length > 150 ? (
          <>{description.slice(0, 150) + "..."}</>
        ) : (
          <>{description}</>
        )}
      </p>
      <div className={styles["card-container-information"]}>
        <p className={styles["card-container-information__time"]}>
          <img src={timer} alt="Durée" />
          {detailSingleFiche.duree_max} min
        </p>
        <p className={styles["card-container-information__difficulty"]}>
          Difficulté
          <>
            {Array(Number(detailSingleFiche.difficulte))
              .fill(null)
              .map((_, i) => (
                <img key={i} src={star_full} alt="étoile pleine" />
              ))}
            {Array(3 - Number(detailSingleFiche.difficulte))
              .fill(null)
              .map((_, i) => (
                <img key={i} src={star_empty} alt="étoile vide" />
              ))}
          </>
        </p>
      </div>
    </div>
  );
}
