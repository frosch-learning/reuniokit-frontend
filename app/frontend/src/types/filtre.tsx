export interface Option {
  label: string;
  value: string;
}

export interface FilterGroup {
  label: string;
  options: Option[];
}

export interface FiltreProps {
  filterGroups: FilterGroup[];
  onFilterChange: (filter: string, groupIndex: number) => void;
}
