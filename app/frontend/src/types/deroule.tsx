import { FicheSynthese } from "./fiche";

export interface DerouleTypeComplement {
  categorie: string;
  contenu: string;
  exemples: string;
  id: number;
  ordre: number;
  precisions: string;
  id_fiche_associe: number;
  fiche_associee?: FicheSynthese;
}

export interface DerouleType {
  nom: string;
  description_longue: string;
  compatible_visio: boolean;
  duree_max: string;
  participant_min: string;
  participant_max: string;
  complement: DerouleTypeComplement[];
}

export interface DerouleTypeSynthese {
  id: string;
  nom: string;
  duree_max: string;
  participant_min: string;
  participant_max: string;
  compatible_visio: boolean;
  description_longue: string;
  url_image: string;
}
