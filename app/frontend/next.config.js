/** @type {import('next').NextConfig} */
const isProd = process.env.NODE_ENV === 'production'
const nextConfig = {
  webpack: config => {

    config.module.rules.push({
      test: /\.woff2$/,
      type: "asset/resource"
    });

    return config;
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  /* typescript: {
    ignoreBuildErrors: true,
  }, */
  assetPrefix: isProd ? '/app' : undefined,
};

module.exports = nextConfig;
