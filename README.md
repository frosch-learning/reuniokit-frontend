### Installer les modules

1. Se déplacer dans la racine du dossier :
   `cd nom_du_dossier`
2. Executer le fichier modules.sh :
   `sh modules.sh`

### Post-installation

Mettre en place les hooks git (formattage automatique du code, ...) :

    npx simple-git-hooks

### Installer les extensions

Sur VSCode,

1. Aller dans Extensions
2. Cliquer sur les 3 points en haut
3. Cliquer sur "Install from vsix"
4. Sélectionner le fichier .vsix (/home/onyxia/work/dossier_de_travail/Extensions/)

### Fichiers static

Refonte de la librairie du design système de l'état
Peut être amené à modifier dans le futur

Pour le build pipeline
